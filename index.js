var rect = require('./rectangle');

function solveRect(l, b) {
    console.log("l = " + l + " b = " + b);
    rect(l, b, (err, rectangle) => {
        if(err) {
            console.log("ERROR: ", err.message);
        }
        else {
            console.log("perimeter = " + rectangle.perimeter() + " and area = " + rectangle.area());
        }
    });
    console.log("I am visible after the call to rect()");
}

solveRect(2, 4);
solveRect(3, 5);
solveRect(0, 5);
solveRect(-3, 5);